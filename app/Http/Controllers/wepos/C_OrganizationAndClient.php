<?php

namespace App\Http\Controllers\wepos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Models\wepos\M_OrganizationAndClient;

class C_OrganizationAndClient extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $M_OrganizationAndClient = new M_OrganizationAndClient();
        $HomeController = new HomeController();
        $user_name = Auth::user()->name;
        $id = Auth::id();
        $myprofil = $HomeController->MyProfil()->sidebar;

        $where1 = array('is_active'=>'t');
        $d_organization = $M_OrganizationAndClient->view_data('w_organizations', $where1)->get();
        $d_client = $M_OrganizationAndClient->view_data('w_clients', $where1)->get();
        
        $data = array(
            'id' => $id,
            'user_name' => $user_name,
            'sidebar' => $myprofil,
            'data_organization' => $d_organization,
            'data_client' => $d_client
        );
        return view('wepos.V_OrganizationAndClient', $data);
    }

    //organization
    public function process_add_organization(Request $request)
    {
        $M_OrganizationAndClient = new M_OrganizationAndClient();
        $created_by = Auth::id();
        $updated_by = Auth::id();
        $organization_value = $request->input('in_organization_value');
        $organization_name = $request->input('in_organization_name');
        $organization_description = $request->input('in_organization_description');
        $data = array(
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => now(),
            'updated_at' => now(),
            'organization_value' => $organization_value,
            'organization_name' => $organization_name,
            'organization_description' => $organization_description
        );
        $M_OrganizationAndClient->add_data_process('w_organizations', $data);
        return redirect(url('organization-and-client'));
    }

    //client
    public function process_add_client(Request $request)
    {
        $M_OrganizationAndClient = new M_OrganizationAndClient();
        $created_by = Auth::id();
        $updated_by = Auth::id();
        $w_organization_id = $request->input('in_w_organization_id');
        $client_value = $request->input('in_client_value');
        $client_name = $request->input('in_client_name');
        $client_description = $request->input('in_client_description');
        $data = array(
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => now(),
            'updated_at' => now(),
            'w_organization_id' => $w_organization_id,
            'client_value' => $client_value,
            'client_name' => $client_name,
            'client_description' => $client_description
        );
        $M_OrganizationAndClient->add_data_process('w_clients', $data);
        return redirect(url('organization-and-client'));
    }

}
