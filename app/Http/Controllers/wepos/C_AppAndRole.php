<?php

namespace App\Http\Controllers\wepos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Models\wepos\M_AppAndRole;

class C_AppAndRole extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $M_AppAndRole = new M_AppAndRole();
        $HomeController = new HomeController();
        $user_name = Auth::user()->name;
        $id = Auth::id();
        $myprofil = $HomeController->MyProfil()->sidebar;

        $where1 = array('is_active'=>'t');
        $d_app = $M_AppAndRole->view_data('w_apps', $where1)->get();
        $d_role = $M_AppAndRole->view_data('w_roles', $where1)->get();
        
        $data = array(
            'id' => $id,
            'user_name' => $user_name,
            'sidebar' => $myprofil,
            'data_app' => $d_app,
            'data_role' => $d_role
        );
        return view('wepos.V_AppAndRole', $data);
    }

    //app
    public function process_add_app(Request $request)
    {
        $M_AppAndRole = new M_AppAndRole();
        $created_by = Auth::id();
        $updated_by = Auth::id();
        $app_value = $request->input('in_app_value');
        $app_name = $request->input('in_app_name');
        $app_description = $request->input('in_app_description');
        $data = array(
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => now(),
            'updated_at' => now(),
            'app_value' => $app_value,
            'app_name' => $app_name,
            'app_description' => $app_description
        );
        $M_AppAndRole->add_data_process('w_apps', $data);
        return redirect(url('app-and-role'));
    }

    //role
    public function process_add_role(Request $request)
    {
        $M_AppAndRole = new M_AppAndRole();
        $created_by = Auth::id();
        $updated_by = Auth::id();
        $w_app_id = $request->input('in_w_app_id');
        $role_value = $request->input('in_role_code');
        $role_name = $request->input('in_role_name');
        $role_description = $request->input('in_role_description');
        $data = array(
            'w_app_id' => $w_app_id,
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => now(),
            'updated_at' => now(),
            'role_value' => $role_value,
            'role_name' => $role_name,
            'role_description' => $role_description
        );
        $M_AppAndRole->add_data_process('w_roles', $data);
        return redirect(url('app-and-role'));
    }
}