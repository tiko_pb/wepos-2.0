<?php

namespace App\Http\Controllers\wepos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Models\wepos\M_User;

class C_User extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $M_User = new M_User();
        $HomeController = new HomeController();
        $user_name = Auth::user()->name;
        $id = Auth::id();
        $myprofil = $HomeController->MyProfil()->sidebar;

        // $where1 = array('is_active'=>'t');
        // $d_organization = $M_OrganizationAndClient->view_data('w_organizations', $where1)->get();
        // $d_client = $M_OrganizationAndClient->view_data('w_clients', $where1)->get();
        
        $data = array(
            'id' => $id,
            'user_name' => $user_name,
            'sidebar' => $myprofil
        );
        return view('wepos.V_User', $data);
    }
    
}
