<?php


namespace App\Http\Controllers\apt1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;


class C_uom extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $HomeController = new HomeController();
        $user_name = Auth::user()->name;
        $id = Auth::id();
        $myprofil = $HomeController->MyProfil()->sidebar;
        $data = array(
            'id' => $id,
            'user_name' => $user_name,
            'sidebar' => $myprofil
        );
        return view('apt1.master-data.V_uom', $data);
    }
}
