<?php

namespace App\Models\wepos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; //add by wbw

class M_OrganizationAndClient extends Model
{
    public function view_data($table, $where)
    {
        return DB::table($table)->where($where);
    }

    public function add_data_process($table, $data)
    {
        DB::table($table)->insert($data);
    }
}
