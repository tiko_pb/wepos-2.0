@extends('layout')

@section('menu')
    @include('wepos.V_Menu')
@endsection 

@section('title','Product')

@section('content')
<!-- Main content -->

<div class="card">   
    <!-- /.card-header -->
    <div class="card-body">

    <div class="row">
        <div class ="col-md-2">
            <button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#Modal-add-product">
                    Add Product
            </button>
        </div>
        <div class ="col-md-2">
            <button type="button" class="btn btn-block btn-info btn-sm" data-toggle="modal" data-target="#Modal-import-product">
                    Import Product
            </button>
        </div>
    </div>
    <br>
    <table id="DataTable" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Satuan</th>
            <th width="20%">Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Trident</td>
        <td>Internet Explorer 4.0</td>
        <td>Tablet</td>
        <td>
            <!-- modal action  start -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Modal-edit-product">
                    <i class="fas fa-pencil-alt">
                    </i>
                        Edit
                </button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Modal-delete-product">
                    <i class="fas fa-trash">
                    </i>
                        Delete
                </button>
            <!-- //modal action end -->
        </td>
        </tr>
        </tbody>        
    </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<!-- /.content -->

<!-- modal edit app start -->
<div class="modal fade" id="Modal-add-product">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Product Name</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
                <div class="row">
                    <div class ="col-md-3">
                        Value
                    </div>
                    <div class ="col-md-6">
                        <input class="form-control" type="text" placeholder="Value">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class ="col-md-3">
                        Product Name
                    </div>
                    <div class ="col-md-6">
                        <input class="form-control" type="text" placeholder="Product Name">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class ="col-md-3">
                        Product Description
                    </div>
                    <div class ="col-md-6">
                        <div class="form-group">
                            <textarea class="form-control" rows="2" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class ="col-md-3">
                        Uom
                    </div>
                    <div class ="col-md-6">
                        <div class="form-group">
                            <select class="form-control">
                            <option>Uom 1</option>
                            <option>Uom 2</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Save</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- modal edit app end -->

<!-- modal delete app start -->
<div class="modal fade" id="Modal-delete-product">
    <div class="modal-dialog">
    <div class="modal-content bg-danger">
        <div class="modal-header">
        <h4 class="modal-title">Delete Product</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <p>Data Product ini akan di hapus!&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-light">Save changes</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- modal delete app end -->      

<!-- modal add app product -->
<div class="modal fade" id="Modal-edit-product">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product Info</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Product Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-uomConvertion" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Uom Convertion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-Harga" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Harga</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-three-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                            <div class="row">
                                <div class ="col-md-3">
                                    Value
                                </div>
                                <div class ="col-md-6">
                                    <input class="form-control" type="text" placeholder="Value" disabled>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class ="col-md-3">
                                    Product Name
                                </div>
                                <div class ="col-md-6">
                                <input class="form-control" type="text" placeholder="Product Name">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class ="col-md-3">
                                    Product Description
                                </div>
                                <div class ="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class ="col-md-3">
                                    Uom
                                </div>
                                <div class ="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                        <option>option 1</option>
                                        <option>option 2</option>
                                        <option>option 3</option>
                                        <option>option 4</option>
                                        <option>option 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-three-uomConvertion" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                            <div class="row">
                                    <div class ="col-md-3" >
                                        Uom From 
                                    </div>
                                <div class ="col-md-6">
                                    <div class="form-group" >
                                        <select class="form-control" disabled>
                                        <option>Uom 1</option>
                                        <option>Uom 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class ="col-md-3">
                                        Uom To 
                                    </div>
                                <div class ="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                        <option>Uom 1</option>
                                        <option>Uom 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class ="col-md-3">
                                    Mutiply Rate
                                </div>
                                <div class ="col-md-6">
                                    <input class="form-control" type="text" placeholder="Mutiply Rate">
                                </div>
                            </div> 
                            <br>
                            <div class="row">
                                <div class ="col-md-3">
                                    Barcode
                                </div>
                                <div class ="col-md-6">
                                    <input class="form-control" type="text" placeholder="Barcode">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-three-Harga" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                            <div class="row">
                                <div class ="col-md-3">
                                        Uom To 
                                    </div>
                                <div class ="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                        <option>Uom 1</option>
                                        <option>Uom 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class ="col-md-3">
                                    Harga Beli
                                </div>
                                <div class ="col-md-6">
                                    <input class="form-control" type="text" placeholder="Rp.">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class ="col-md-3">
                                    Harga Jual
                                </div>
                                <div class ="col-md-6">
                                    <input class="form-control" type="text" placeholder="Rp.">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.card -->
        </div>
       
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- modal add app product -->

@endsection