@extends('layout')

@section('menu')
    @include('wepos.V_Menu')
@endsection 

@section('title','UOM')


@section('content')
<!-- Main content -->
<div class="card">   
    <!-- /.card-header -->
    <div class="card-body">
    <div class ="col-md-2">
    <button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#Modal-add-product">
                  Add Uom
    </button>
    </div>
    <br>
    <table id="DataTable" class="table table-bordered table-striped">
        <thead>
            <th>Product Name</th>
            <th>Description</th>
            <th>Satuan</th>
            <th width="20%">Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Trident</td>
        <td>Internet Explorer 4.0</td>
        <td>Tablet</td>
        <td>
            <!-- modal action  start -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#Modal-edit-product-category">
                    <i class="fas fa-pencil-alt">
                    </i>
                        Edit
                </button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-product-category">
                    <i class="fas fa-trash">
                    </i>
                        Delete
                </button>
            <!-- //modal action end -->
        </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
        <th>Product Name</th>
        <th>Description</th>
        <th>Satuan</th>
        <th>Action</th>
        </tr>
        </tfoot>
    </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<!-- /.content -->

<!-- modal add app product -->
<div class="modal fade" id="Modal-add-product">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">ADD PRODUCT</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Save</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- modal add app product -->

<!-- modal add app start -->
<div class="modal fade" id="Modal-edit-product-category">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Product Name</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary">Save</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- modal add app end -->

<!-- modal delete app start -->
<div class="modal fade" id="delete-product-category">
    <div class="modal-dialog">
    <div class="modal-content bg-danger">
        <div class="modal-header">
        <h4 class="modal-title">Delete Product</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <p>Data Product ini akan di hapus!&hellip;</p>
        </div>
        <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-light">Save changes</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- modal delete app end -->      

@endsection