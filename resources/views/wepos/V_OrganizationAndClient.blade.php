@extends('layout')

@section('menu')
  @include('wepos/V_Menu')
@endsection

@section('title','Organization And Client')

@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
			<div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Organizations</h5>
                    </div>
                    <div class="card-body">
                        <div class='row'>
                            <div class="col-lg-2">
                                <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-organization"><i class='fas fa-plus-circle'></i> New Organization</a>
                            </div>    
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="DataTable-Organization" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="background-color:#66A1D2;" width="5%">No</th>
                                        <th style="background-color:#66A1D2;" width="25%">App Code</th>
                                        <th style="background-color:#66A1D2;" width="25%">App Name</th>
                                        <th style="background-color:#66A1D2;" width="30%">Description</th>
                                        <th style="background-color:#66A1D2;" width="15%">Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no=1;
                                        foreach ($data_organization as $org) {
                                    ?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $org->organization_value }}</td>
                                        <td>{{ $org->organization_name }}</td>
                                        <td>{{ $org->organization_description }}</td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
			<div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Clients</h5>
                    </div>
                    <div class="card-body">
                        <div class='row'>
                            <div class="col-lg-2">
                                <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-client"><i class='fas fa-plus-circle'></i> New Client</a>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="DataTable-Client" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="background-color:#66A1D2;" width="5%">No</th>
                                        <th style="background-color:#66A1D2;" width="25%">App Code</th>
                                        <th style="background-color:#66A1D2;" width="25%">App Name</th>
                                        <th style="background-color:#66A1D2;" width="30%">Description</th>
                                        <th style="background-color:#66A1D2;" width="15%">Option</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $no=1;
                                        foreach ($data_client as $cli) {
                                    ?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $cli->client_value }}</td>
                                        <td>{{ $cli->client_name }}</td>
                                        <td>{{ $cli->client_description }}</td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /.container-fluid -->
</div><!-- /.content -->

<!-- Modal Add Organization -->
<div class="modal fade" id="modal-add-organization">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Organization</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('process-add-organization') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="in_organization_code">Organization Code</label>
                    <input type="text" name="in_organization_value" class="form-control" id="in_organization_code" placeholder="Organization Code" required>
                </div>
                <div class="form-group">
                    <label for="in_organization_name">Organization Name</label>
                    <input type="text" name="in_organization_name" class="form-control" id="in_organization_name" placeholder="Organization Name" required>
                </div>
                <div class="form-group">
                    <label for="in_organization_description">Description</label>
                    <textarea name="in_organization_description" class="form-control rounded-0" id="in_organization_description" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Modal Add Organization -->

<!-- Modal Add Client -->
<div class="modal fade" id="modal-add-client">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Organization</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('process-add-client') }}" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="in_w_organization_id">Organization</label>
                        <select name="in_w_organization_id" class="form-control" placeholder="Organization" required>
                            <option value="">Chose Organization</option>
                                <?php
                                    foreach ($data_organization as $dorg) {
                                ?>
                                    <option value="{{ $dorg->w_organization_id }}">{{ $dorg->organization_name }}</option>
                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="in_client_code">Client Code</label>
                        <input type="text" name="in_client_value" class="form-control" id="in_client_code" placeholder="Client Code" required>
                    </div>
                    <div class="form-group">
                        <label for="in_client_name">Client Name</label>
                        <input type="text" name="in_client_name" class="form-control" id="in_client_name" placeholder="Client Name" required>
                    </div>
                    <div class="form-group">
                        <label for="in_client_description">Description</label>
                        <textarea name="in_client_description" class="form-control rounded-0" id="in_client_description" rows="4"></textarea>
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Modal Add Client -->


@endsection