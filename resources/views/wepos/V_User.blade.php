@extends('layout')

@section('menu')
  @include('wepos/V_Menu')
@endsection

@section('title','Users')

@section('content')
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
			<div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Users</h5>
                    </div>
                    <div class="card-body">
                        <div class='row'>
                            <div class="col-lg-2">
                                <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-user"><i class='fas fa-plus-circle'></i> New User</a>
                            </div>    
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</div><!-- /.content -->

<!-- Modal Add User -->
<div class="modal fade" id="modal-add-user">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('process-add-user') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="in_user_code">User Code</label>
                    <input type="text" name="in_user_value" class="form-control" id="in_user_code" placeholder="User Code" required>
                </div>
                <div class="form-group">
                    <label for="in_user_name">User Name</label>
                    <input type="text" name="in_user_name" class="form-control" id="in_user_name" placeholder="User Name" required>
                </div>
                <div class="form-group">
                    <label for="in_password">Password</label>
                    <input type="text" name="in_password" class="form-control" id="in_password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="in_user_description">Description</label>
                    <textarea name="in_user_description" class="form-control rounded-0" id="in_user_description" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Modal Add Organization -->

@endsection