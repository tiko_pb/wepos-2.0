@extends('layout')

@section('menu')
  @include('wepos/V_Menu')
@endsection

@section('title','App & Role')

@section('content')
<!-- Main content -->
<div class="content">
  <div class="container-fluid">

    <div class="row">
			<div class="col-lg-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h5 class="m-0">Apps</h5>
          </div>
          <div class="card-body">
            <div class='row'>
              <div class="col-lg-2">
                <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-app"><i class='fas fa-plus-circle'></i> New App</a>
              </div>    
            </div>
            <br>
            <div class="table-responsive">
              <table id="DataTable-App" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="background-color:#66A1D2;" width="5%">No</th>
                    <th style="background-color:#66A1D2;" width="25%">App Code</th>
                    <th style="background-color:#66A1D2;" width="25%">App Name</th>
                    <th style="background-color:#66A1D2;" width="30%">Description</th>
                    <th style="background-color:#66A1D2;" width="15%">Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
		                $no=1;
		                foreach ($data_app as $dapp) {
                  ?>
                  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $dapp->app_value }}</td>
                    <td>{{ $dapp->app_name }}</td>
                    <td>{{ $dapp->app_description }}</td>
                    <td></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
	    </div>
    </div>
    <!-- /.row -->

    <div class="row">
			<div class="col-lg-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h5 class="m-0">Roles</h5>
          </div>
          <div class="card-body">
            <div class='row'>
              <div class="col-lg-2">
                <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add-role"><i class='fas fa-plus-circle'></i> New Role</a>
              </div>    
            </div>
            <br>
            <div class="table-responsive">
              <table id="DataTable-Role" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="background-color:#66A1D2;" width="5%">No</th>
                    <th style="background-color:#66A1D2;" width="25%">App Code</th>
                    <th style="background-color:#66A1D2;" width="25%">App Name</th>
                    <th style="background-color:#66A1D2;" width="30%">Description</th>
                    <th style="background-color:#66A1D2;" width="15%">Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
		                $no=1;
		                foreach ($data_role as $drole) {
                  ?>
                  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $drole->role_value }}</td>
                    <td>{{ $drole->role_name }}</td>
                    <td>{{ $drole->role_description }}</td>
                    <td></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
			</div>
    </div>
    <!-- /.row -->

  </div><!-- /.container-fluid -->
</div><!-- /.content -->

<!-- Modal Add App -->
  <div class="modal fade" id="modal-add-app">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add App</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <form action="{{ url('process-add-app') }}" method="POST">
            @csrf
              <div class="form-group">
                <label for="in_app_code">App Code</label>
                <input type="text" name="in_app_value" class="form-control" id="in_app_code" placeholder="App Code" required>
              </div>
              <div class="form-group">
                <label for="in_app_name">App Name</label>
                <input type="text" name="in_app_name" class="form-control" id="in_app_name" placeholder="App Name" required>
              </div>
              <div class="form-group">
                <label for="in_app_description">Description</label>
                <textarea name="in_app_description" class="form-control rounded-0" id="in_app_description" rows="4"></textarea>
              </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-primary" type="submit">Save</button>
        </div>
            </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
<!-- Modal Add App -->

<!-- Modal Add Role -->
<div class="modal fade" id="modal-add-role">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Role</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/process-add-role" method="POST">
          @csrf
            <div class="form-group">
              <label for="in_w_app_id">App</label>
              <select name="in_w_app_id" class="form-control" placeholder="App" required>
                <option value="">Chose App</option>
                <?php
                  foreach ($data_app as $dapp) {
                ?>
                  <option value="{{ $dapp->w_app_id }}">{{ $dapp->app_name }}</option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="in_role_code">Role Code</label>
              <input type="text" name="in_role_code" class="form-control" id="in_role_code" placeholder="Role Code">
            </div>
            <div class="form-group">
              <label for="in_role_name">Role Name</label>
                <input type="text" name="in_role_name" class="form-control" id="in_role_name" placeholder="Role Name">
            </div>
            <div class="form-group">
              <label for="in_role_description">Description</label>
              <textarea name="in_role_description" class="form-control rounded-0" id="in_role_description" rows="4"></textarea>
            </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Save</button>
      </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- Modal Add Role -->
@endsection