<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableWClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_clients', function (Blueprint $table) {
            $table->bigIncrements('w_client_id');
            $table->bigInteger('w_organization_id');
            $table->boolean('is_active')->default(true);
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->string('client_value', 100)->nullable();
            $table->string('client_name', 100)->nullable();
            $table->string('client_description', 255)->nullable();

            $table->foreign('w_organization_id')->references('w_organization_id')->on('w_organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_clients');
    }
}
