<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableWApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_apps', function (Blueprint $table) {
            $table->bigIncrements('w_app_id');
            $table->boolean('is_active')->default(true);
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->string('app_value', 100)->nullable();
            $table->string('app_name', 100)->nullable();
            $table->string('app_description', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_apps');
    }
}
