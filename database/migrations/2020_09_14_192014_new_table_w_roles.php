<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTableWRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_roles', function (Blueprint $table) {
            $table->bigIncrements('w_role_id');
            $table->bigInteger('w_app_id');
            $table->boolean('is_active')->default(true);
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->string('role_value', 100)->nullable();
            $table->string('role_name', 100)->nullable();
            $table->string('role_description', 255)->nullable();

            $table->foreign('w_app_id')->references('w_app_id')->on('w_apps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_roles');
    }
}
