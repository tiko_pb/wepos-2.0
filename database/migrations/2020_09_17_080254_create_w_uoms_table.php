<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWUomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_uoms', function (Blueprint $table) {
            $table->bigIncrements('w_uom_id');
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->decimal('w_organization_id', 10, 0);
            $table->decimal('w_client_id', 10, 0);

            $table->string('uom_name', 100)->nullable();
            $table->string('uom_description', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_uoms');
    }
}
