<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_products', function (Blueprint $table) {
            $table->bigIncrements('w_product_id');
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->decimal('w_organization_id', 10, 0);
            $table->decimal('w_client_id', 10, 0);

            $table->string('product_value', 100)->nullable();
            $table->string('product_name', 100)->nullable();
            $table->string('product_description', 255)->nullable();
            $table->bigInteger('w_product_category_id');
            $table->bigInteger('w_uom_id');
            $table->string('product_barcode', 100)->nullable();
            
            // colom table awal -> reference ke kolom apa -> pada table apa 
            $table->foreign('w_product_category_id')->references('w_product_category_id')->on('w_product_categorys');
            $table->foreign('w_uom_id')->references('w_uom_id')->on('w_uoms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_products');
    }
}
