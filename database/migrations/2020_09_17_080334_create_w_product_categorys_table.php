<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWProductCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_product_categorys', function (Blueprint $table) {
            $table->bigIncrements('w_product_category_id');
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->decimal('w_organization_id', 10, 0);
            $table->decimal('w_client_id', 10, 0);

            $table->string('pc_value', 100)->nullable();
            $table->string('pc_name', 100)->nullable();
            $table->string('pc_description', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_product_categorys');
    }
}
