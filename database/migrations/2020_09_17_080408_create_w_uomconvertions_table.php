<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWUomconvertionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_uomconvertions', function (Blueprint $table) {
            $table->bigIncrements('w_uomconvertion_id');
            $table->decimal('created_by', 10, 0)->nullable();
            $table->decimal('updated_by', 10, 0)->nullable();
            $table->timestamps();
            $table->decimal('w_organization_id', 10, 0);
            $table->decimal('w_client_id', 10, 0);

            $table->bigInteger('w_uom_id');
            $table->bigInteger('w_uomto_id');
            $table->decimal('mutiply_rate', 10, 0)->nullable();
            
            // colom table awal -> reference ke kolom apa -> pada table apa 
            $table->foreign('w_uom_id')->references('w_uom_id')->on('w_uoms');
            $table->foreign('w_uomto_id')->references('w_uom_id')->on('w_uoms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_uomconvertions');
    }
}
