<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('login'); });


Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::post('/update-collapse', 'HomeController@Update_Collapse');
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home-superadmin', 'wepos\C_Home@index')->name('home-superadmin');
    
    //Setting App
    Route::get('/app-and-role', 'wepos\C_AppAndRole@index')->name('app-and-role');
        //app
        Route::post('process-add-app', 'wepos\C_AppAndRole@process_add_app')->name('process-add-app');

        //role
        Route::post('process-add-role', 'wepos\C_AppAndRole@process_add_role');

    Route::get('/organization-and-client', 'wepos\C_OrganizationAndClient@index')->name('organization-and-client');
        //organization
        Route::post('process-add-organization', 'wepos\C_OrganizationAndClient@process_add_organization');

        //client
        Route::post('process-add-client', 'wepos\C_OrganizationAndClient@process_add_client');

        Route::get('/user', 'wepos\C_user@index')->name('user');
    
    //master data 
    Route::get('/master-data/product', 'apt1\C_product@index');
    Route::get('/master-data/product-category', 'apt1\C_product_category@index');
    Route::get('/master-data/uom', 'apt1\C_uom@index');
});